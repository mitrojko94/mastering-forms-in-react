const JSONPreview = ({ data }) => {
  return (
    <pre>
      <code lang="json">{JSON.stringify(data, null, 2)}</code>
    </pre>
  );
};

export default JSONPreview;
