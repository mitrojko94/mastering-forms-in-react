import React, { useEffect, useState } from "react";
import JSONPreview from "./JSONPreview";

const FormComponent = () => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [age, setAge] = useState("");
  const [employee, setEmployee] = useState(false);
  const [favoriteColor, setFavoriteColor] = useState("");
  const [sauces, setSauces] = useState([]);
  const [stooge, setStooge] = useState("Larry");
  const [notes, setNotes] = useState("");
  const [error, setError] = useState(false);

  const handleSaucesChange = (e) => {
    setSauces(() =>
      e.target.checked
        ? [...sauces, e.target.value]
        : sauces.filter((s) => s !== e.target.value)
    );
  };

  const errorSetting = (value) => {
    !value
      .split("")
      .map((el) => (el === "" ? true : isNaN(el)))
      .includes(false)
      ? setError("Must use characters")
      : setError(null);
  };

  const errorSettingLastName = (value) => {
    !value
      .split("")
      .map((el) => (el === "" ? true : isNaN(el)))
      .includes(false)
      ? setError("Must use characters")
      : setError(null);
  };

  // useEffect for age validation
  useEffect(() => {
    if (isNaN(age)) {
      setError("You age must be number");
    } else {
      setError(false);
    }
  }, [age]);

  // useEffect for notes validation
  useEffect(() => {
    if (notes.length >= 100) {
      setError("You text can't be more than 100!");
    } else {
      setError(null);
    }
  }, [notes]);

  const formTouched = () => {
    return Boolean(
      firstName ||
        lastName ||
        age ||
        employee ||
        favoriteColor ||
        sauces.length > 0 ||
        notes
    );
  };

  const handleClick = (e) => {
    e.preventDefault();
    if (
      firstName !== "" &&
      lastName !== "" &&
      age !== "" &&
      employee !== false &&
      favoriteColor !== "" &&
      sauces !== "" &&
      stooge !== "" &&
      notes !== ""
    ) {
      alert(
        `${firstName} ${lastName} ${age} ${employee} ${favoriteColor} ${sauces} ${notes}`
      );
    }
  };

  const reset = () => {
    setFirstName("");
    setLastName("");
    setAge("");
    setEmployee(false);
    setFavoriteColor("");
    setSauces([]);
    setStooge("Larry");
    setNotes("");
  };

  return (
    <form>
      <div className="firstName">
        <label htmlFor="firstName">First Name </label>
        <input
          type="text"
          id="firstName"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          style={{ borderColor: errorSetting ? "black" : "red" }}
        />
      </div>
      <div className="lastName">
        <label htmlFor="lastName">Last Name </label>
        <input
          type="text"
          id="lastName"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          style={{ borderColor: errorSettingLastName ? "black" : "red" }}
        />
      </div>
      <div className="age">
        <label htmlFor="age">Age </label>
        <input
          type="text"
          id="age"
          placeholder="Age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
          style={{ borderColor: error ? "red" : "black" }}
        />
      </div>
      <div className="employee">
        <label htmlFor="employee">Employee </label>
        <input
          type="checkbox"
          name="employee"
          id="employee"
          value={employee}
          checked={employee}
          onChange={(e) => setEmployee(e.target.checked)}
        />
      </div>
      <div className="favoriteColor">
        <label htmlFor="favoriteColor">Favorite Color </label>
        <select
          name="favoriteColor"
          id="favoriteColor"
          value={favoriteColor}
          onChange={(e) => setFavoriteColor(e.target.value)}
        >
          <option value="#"></option>
          <option value="green">green</option>
          <option value="red">red</option>
          <option value="yellow">yellow</option>
        </select>
      </div>
      <div className="sauces">
        <label htmlFor="sauces">Sauces </label>
        <ul>
          <li>
            <input
              type="checkbox"
              name="sauces"
              id="sauces"
              value="Ketchup"
              checked={sauces.includes("Ketchup")}
              onChange={handleSaucesChange}
            />
            Ketchup
          </li>
          <li>
            <input
              type="checkbox"
              name="sauces"
              id="sauces"
              value="Mustard"
              checked={sauces.includes("Mustard")}
              onChange={handleSaucesChange}
            />
            Mustard
          </li>
          <li>
            <input
              type="checkbox"
              name="sauces"
              id="sauces"
              value="Mayonnaise"
              checked={sauces.includes("Mayonnaise")}
              onChange={handleSaucesChange}
            />
            Mayonnaise
          </li>
          <li>
            <input
              type="checkbox"
              name="sauces"
              id="sauces"
              value="Guacamole"
              checked={sauces.includes("Guacamole")}
              onChange={handleSaucesChange}
            />
            Guacamole
          </li>
        </ul>
      </div>
      <div className="stooge">
        <label htmlFor="stooge">Best Stooge </label>
        <ul>
          <li>
            <input
              type="radio"
              name="stooge"
              id="stooge"
              checked={stooge === "Larry"}
              value="Larry"
              onChange={(e) => setStooge(e.target.value)}
            />
            Larry
          </li>
          <li>
            <input
              type="radio"
              name="stooge"
              id="stooge"
              checked={stooge === "Moe"}
              value="Moe"
              onChange={(e) => setStooge(e.target.value)}
            />
            Moe
          </li>
          <li>
            <input
              type="radio"
              name="stooge"
              id="stooge"
              checked={stooge === "Curly"}
              value="Curly"
              onChange={(e) => setStooge(e.target.value)}
            />
            Curly
          </li>
        </ul>
      </div>
      <div className="notes">
        <label htmlFor="notes">Notes </label>
        <textarea
          name="notes"
          id="notes"
          cols="30"
          rows="2"
          placeholder="Notes"
          value={notes}
          onChange={(e) => setNotes(e.target.value)}
          style={{ borderColor: Boolean(error) ? "red" : "black" }}
        ></textarea>
      </div>
      <div className="buttons">
        <button
          type="submit"
          className="submit"
          onClick={handleClick}
          disabled={!formTouched()}
        >
          Submit
        </button>
        <button
          type="button"
          onClick={reset}
          className="reset"
          disabled={!formTouched()}
        >
          Reset
        </button>
      </div>
      <div className="content">
        <JSONPreview
          data={{
            firstName,
            lastName,
            age,
            employee,
            favoriteColor,
            sauces,
            stooge,
            notes,
          }}
        />
      </div>
    </form>
  );
};

export default FormComponent;
